/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include <vulkanio.hpp>
#include <render.hpp>
#include <wayland.hpp>

#include <iostream>
#include <ctime>
#include <map>

namespace vkc
{

bool VulkanIO_running = true;

class VulkanIO::impl
{
      public:
        ~impl() { Shutdown(); };

        bool Init(VulkanIO *v)
        {
                m_this = v;
                return CreateInstance() && FindPhysicalDevice();
        }

        void Shutdown()
        {
                for (auto it = m_devices.begin(); it != m_devices.end(); ++it)
                        if ((*it)->m_logical)
                                (*it)->m_logical.destroy();

                if (singleton_instance)
                        singleton_instance.destroy();
        }

        void PollDisplays()
        {
                for (auto it = m_devices.begin(); it != m_devices.end(); ++it)
                {
#ifdef VKW_HAVE_VK_REGISTER_DEVICE_EVENT_EXT
                        m_this->status = (*it)->m_logical.getFenceStatus(it->m_deviceEventFence);
                        if (m_this->status == vk::Result::eSuccess)
                        {
                                (*it)->m_logical.resetFences(1, &(*it)->m_deviceEventFence);
                                (*it)->EnumerateDisplays()
                        }
#else
                        if ((*it)->init)
                        {
                                (*it)->EnumerateDisplays();
                                (*it)->init = false;
                        }
#endif
                }
        }

        bool IsValid()
        {
                return VulkanIO_running;
        }

      private:
        VulkanIO *m_this;

        class Device
        {
              public:
                ~Device() { Shutdown(); }

                bool Init(vk::PhysicalDevice p, vk::Device l, vk::Result *s)
                {
                        m_physical = p;
                        m_logical = l;
                        m_status = s;

#ifdef VKW_HAVE_VK_REGISTER_DEVICE_EVENT_EXT
                        vk::DeviceEventInfoEXT DeviceEventInfoEXT;
                        DeviceEventInfoEXT.setDeviceEvent(vk::DeviceEventTypeEXT::eDisplayHotplug);
                        std::tie(m_this->status, m_deviceEventFence) = m_logical.registerEventEXT(DeviceEventInfoEXT);
                        if (m_this->status != vk::Result::eSuccess)
                        {
                                std::cerr << "Failed to create Device Event Fence." << std::endl;
                                return false;
                        }
#else
                        init = true;
#endif
                        return true;
                }

                void Shutdown()
                {
                        m_displays.clear();

                        if (m_logical)
                                m_logical.destroy();
                }

                class Display
                {
                      public:
                        bool Init(vk::PhysicalDevice p, vk::DisplayPropertiesKHR &displayProperties, vk::Result *s)
                        {

                                std::vector<vk::DisplayModePropertiesKHR> modeProperties;
                                std::tie(*s, modeProperties) = p.getDisplayModePropertiesKHR(displayProperties.display);
                                if (*s != vk::Result::eSuccess)
                                {
                                        std::cerr << "Failed to find video mode" << std::endl;
                                        return false;
                                }

                                if (modeProperties.empty())
                                {
                                        std::cerr << "No video modes" << std::endl;
                                        return false;
                                }

                                m_modeProperties = modeProperties[0];

                                std::vector<vk::DisplayPlanePropertiesKHR> planeProperties;
                                std::tie(*s, planeProperties) = p.getDisplayPlanePropertiesKHR();
                                for (uint32_t i = 0; i < planeProperties.size(); ++i)
                                {
                                        if ((planeProperties[i].currentDisplay != vk::DisplayKHR()) &&
                                            (planeProperties[i].currentDisplay != displayProperties.display))
                                                continue;

                                        std::vector<vk::DisplayKHR> supportedDisplays;
                                        std::tie(*s, supportedDisplays) = p.getDisplayPlaneSupportedDisplaysKHR(i);
                                        if (*s != vk::Result::eSuccess)
                                        {
                                                std::cerr << "Failed get supported devices for panel " << i << std::endl;
                                                continue;
                                        }

                                        for (auto it : supportedDisplays)
                                        {
                                                if (it != displayProperties.display)
                                                        continue;

                                                vk::DisplayPlaneCapabilitiesKHR planeCaps;
                                                std::tie(*s, planeCaps) = p.getDisplayPlaneCapabilitiesKHR(m_modeProperties.displayMode, i);
                                                vk::DisplayPlaneAlphaFlagBitsKHR alphaMode;
                                                vk::DisplayPlaneAlphaFlagBitsKHR alphaModes[] = {
                                                    vk::DisplayPlaneAlphaFlagBitsKHR::eOpaque,
                                                    vk::DisplayPlaneAlphaFlagBitsKHR::eGlobal,
                                                    vk::DisplayPlaneAlphaFlagBitsKHR::ePerPixel,
                                                    vk::DisplayPlaneAlphaFlagBitsKHR::ePerPixelPremultiplied,
                                                };
                                                for (uint32_t i = 0; i < sizeof(alphaModes); i++)
                                                {
                                                        if (planeCaps.supportedAlpha & alphaModes[i])
                                                        {
                                                                alphaMode = alphaModes[i];
                                                                break;
                                                        }
                                                }

                                                vk::DisplaySurfaceCreateInfoKHR displayInfo;
                                                displayInfo.setDisplayMode(m_modeProperties.displayMode);
                                                displayInfo.setPlaneIndex(i);
                                                displayInfo.setPlaneStackIndex(planeProperties[i].currentStackIndex);
                                                displayInfo.setTransform(vk::SurfaceTransformFlagBitsKHR::eIdentity);
                                                displayInfo.setAlphaMode(alphaMode);
                                                displayInfo.setGlobalAlpha(1.f);
                                                displayInfo.setImageExtent(m_modeProperties.parameters.visibleRegion);

                                                std::tie(*s, surface) = singleton_instance.createDisplayPlaneSurfaceKHR(displayInfo);
                                                if (*s != vk::Result::eSuccess)
                                                {
                                                        std::cerr << "Failed to create panel surface" << std::endl;
                                                        return false;
                                                }

                                                return true;
                                        }
                                }

                                std::cerr << "No supported plane" << std::endl;
                                return false;
                        }

                        vk::SurfaceKHR surface;
                        vk::DisplayModePropertiesKHR m_modeProperties;
                        std::time_t atime;
                };
                std::map<vk::DisplayKHR, std::unique_ptr<Display>> m_displays;

                vk::Device m_logical;
#ifdef VKW_HAVE_VK_REGISTER_DEVICE_EVENT_EXT
                vk::Fence m_deviceEventFence;
#else
                bool init;
#endif

                void EnumerateDisplays()
                {
                        std::time_t now;
                        std::vector<vk::DisplayPropertiesKHR> displayProperties;
                        std::tie(*m_status, displayProperties) = m_physical.getDisplayPropertiesKHR();
                        if (*m_status != vk::Result::eSuccess)
                        {
                                std::cerr << "Failed to get displays" << std::endl;
                                return;
                        }

                        if (displayProperties.empty())
                        {
                                std::cerr << "No displays found" << std::endl;
                                return;
                        }

                        std::time(&now);
                        for (auto it = displayProperties.begin(); it != displayProperties.end(); ++it)
                        {
                                if (0 == m_displays.count(it->display))
                                {
                                        auto display = new Display();
                                        if (display->Init(m_physical, *it, m_status))
                                        {
                                                m_displays.insert(std::make_pair(it->display, std::unique_ptr<Display>(display)));
                                                singleton_render->InsertSurface(m_logical, display->surface);
                                                display->atime = now;
                                        }
                                        else
                                                delete display;
                                }
                                else
                                        m_displays[it->display]->atime = now;
                        }
                        for (auto it = m_displays.begin(); it != m_displays.end(); ++it)
                        {
                                if (it->second->atime != now)
                                {
                                        singleton_render->RemoveSurface(m_logical, it->second->surface);
                                        m_displays.erase(it);
                                }
                        }
                }

              private:
                vk::PhysicalDevice m_physical;
                vk::Result *m_status;
        };
        std::vector<std::unique_ptr<Device>> m_devices;

        bool CreateInstance()
        {
                std::vector<char const *> wantInstanceLayers;
                std::vector<char const *> wantInstanceExtensions{
                    VK_KHR_DISPLAY_EXTENSION_NAME,
                    VK_KHR_SURFACE_EXTENSION_NAME};

#ifndef NDEBUG
                wantInstanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
                wantInstanceLayers.push_back("VK_LAYER_LUNARG_standard_validation");
#endif

                std::vector<vk::LayerProperties> layers;
                std::tie(m_this->status, layers) = vk::enumerateInstanceLayerProperties();
                if (m_this->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to enumerateInstanceLayerProperties." << std::endl;
                        return false;
                }

                std::vector<char const *> enabledInstanceLayers;
                for (std::vector<vk::LayerProperties>::const_iterator x = layers.begin(); x != layers.end(); ++x)
                        for (std::vector<char const *>::const_iterator y = wantInstanceLayers.begin(); y != wantInstanceLayers.end(); ++y)
                                if (0 == strcmp(x->layerName, *y))
                                        enabledInstanceLayers.push_back(*y);

                std::vector<vk::ExtensionProperties> extentions;
                std::tie(m_this->status, extentions) = vk::enumerateInstanceExtensionProperties();
                if (m_this->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to enumerateInstanceExtensionProperties." << std::endl;
                        return false;
                }

                std::vector<char const *> enabledInstanceExtensions;
                for (std::vector<vk::ExtensionProperties>::const_iterator x = extentions.begin(); x != extentions.end(); ++x)
                        for (std::vector<char const *>::const_iterator y = wantInstanceExtensions.begin(); y != wantInstanceExtensions.end(); ++y)
                                if (0 == strcmp(x->extensionName, *y))
                                {
                                        enabledInstanceExtensions.push_back(*y);
                                        m_this->extentions.insert(*y);
                                }

#ifndef NDEBUG
                if (0)
                        for (std::vector<char const *>::const_iterator instance = enabledInstanceExtensions.begin(); instance != enabledInstanceExtensions.end(); ++instance)
                                std::cout << *instance << std::endl;
#endif

                vk::ApplicationInfo appInfo;
                appInfo.setApiVersion(VK_API_VERSION_1_1);
                appInfo.setApplicationVersion(m_this->applicationVersion);
                appInfo.setPApplicationName(m_this->applicationName.c_str());
                appInfo.setPEngineName("VkWaylandFbCon");
                appInfo.setEngineVersion(VK_MAKE_VERSION(0, 0, 1));

                vk::InstanceCreateInfo instanceCreateInfo;
                instanceCreateInfo.setPApplicationInfo(&appInfo);
                instanceCreateInfo.setEnabledLayerCount(static_cast<uint32_t>(enabledInstanceLayers.size()));
                instanceCreateInfo.setPpEnabledLayerNames(enabledInstanceLayers.data());
                instanceCreateInfo.setEnabledExtensionCount(static_cast<uint32_t>(enabledInstanceExtensions.size()));
                instanceCreateInfo.setPpEnabledExtensionNames(enabledInstanceExtensions.data());

                std::tie(m_this->status, singleton_instance) = vk::createInstance(instanceCreateInfo);
                if (m_this->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create Vulkan instance." << std::endl;
                        return false;
                }

                return true;
        }

        bool hasDMABUF = true;
        bool FindPhysicalDevice()
        {
                std::vector<vk::PhysicalDevice> physicalDevices;
                std::tie(m_this->status, physicalDevices) = singleton_instance.enumeratePhysicalDevices();
                if (m_this->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to find Vulkan capable devices." << std::endl;
                        return false;
                }

                for (auto physicalDevice : physicalDevices)
                {
                        auto const queueFamilyProperties = physicalDevice.getQueueFamilyProperties();

                        for (uint32_t instance = 0; instance < queueFamilyProperties.size(); ++instance)
                        {
                                if ((queueFamilyProperties[instance].queueFlags & vk::QueueFlagBits::eGraphics))
                                {
                                        CreateLogicalDevice(physicalDevice, instance);
                                }
                        }
                }

                if (m_devices.empty())
                {
                        m_this->status = vk::Result::eErrorInitializationFailed;
                        std::cerr << "Failed to find discrete gpu." << std::endl;
                        return false;
                }

                if (hasDMABUF && singleton_render->InitDMABUF())
                        singleton_wayland->m_DmaBuf.Init();

                return true;
        }

        void CreateLogicalDevice(vk::PhysicalDevice physical, uint32_t familyIndex)
        {
                vk::DeviceQueueCreateInfo deviceQueueCreateInfo;
                float const priority = 1.0f;
                deviceQueueCreateInfo.setPQueuePriorities(&priority);
                deviceQueueCreateInfo.setQueueCount(1);
                deviceQueueCreateInfo.setQueueFamilyIndex(familyIndex);

                std::vector<char const *> wantExtensionNames = {
                    VK_KHR_DISPLAY_SWAPCHAIN_EXTENSION_NAME,
                    VK_EXT_DISPLAY_CONTROL_EXTENSION_NAME,
                    VK_EXT_DISPLAY_SURFACE_COUNTER_EXTENSION_NAME,
                    VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME,
                    VK_KHR_MAINTENANCE1_EXTENSION_NAME,
                    VK_KHR_SAMPLER_YCBCR_CONVERSION_EXTENSION_NAME,
                    VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME,
                    VK_KHR_IMAGE_FORMAT_LIST_EXTENSION_NAME,
                    VK_KHR_BIND_MEMORY_2_EXTENSION_NAME,
                    VK_EXT_IMAGE_DRM_FORMAT_MODIFIER_EXTENSION_NAME,
                    VK_KHR_EXTERNAL_MEMORY_CAPABILITIES_EXTENSION_NAME,
                    VK_KHR_EXTERNAL_MEMORY_EXTENSION_NAME,
                    VK_KHR_EXTERNAL_MEMORY_FD_EXTENSION_NAME,
                    VK_EXT_EXTERNAL_MEMORY_DMA_BUF_EXTENSION_NAME,
                    VK_KHR_SWAPCHAIN_EXTENSION_NAME};

                std::vector<vk::ExtensionProperties> extentions;
                std::tie(m_this->status, extentions) = physical.enumerateDeviceExtensionProperties();
                if (m_this->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to enumerateDeviceExtensionProperties." << std::endl;
                        return;
                }

                std::set<std::string> device_extentions;
                std::vector<char const *> deviceExtensionNames;
                for (std::vector<vk::ExtensionProperties>::const_iterator x = extentions.begin(); x != extentions.end(); ++x)
                        for (std::vector<char const *>::const_iterator y = wantExtensionNames.begin(); y != wantExtensionNames.end(); ++y)
                                if (0 == strcmp(x->extensionName, *y))
                                {
                                        deviceExtensionNames.push_back(*y);
                                        device_extentions.insert(*y);
                                }

                vk::DeviceCreateInfo deviceCreateInfo;
                deviceCreateInfo.setQueueCreateInfoCount(1);
                deviceCreateInfo.setPQueueCreateInfos(&deviceQueueCreateInfo);
                deviceCreateInfo.setEnabledExtensionCount(deviceExtensionNames.size());
                deviceCreateInfo.setPpEnabledExtensionNames(deviceExtensionNames.data());

                vk::Device logical;
                std::tie(m_this->status, logical) = physical.createDevice(deviceCreateInfo);
                if (m_this->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create logical device." << std::endl;
                }
                else
                {
                        auto device = new Device();
                        if (device->Init(physical, logical, &m_this->status))
                        {
                                m_devices.insert(m_devices.end(), std::unique_ptr<Device>(device));
                                singleton_render->CreateDevice(physical, logical, familyIndex, device_extentions);
                                hasDMABUF = hasDMABUF && device_extentions.count(VK_EXT_EXTERNAL_MEMORY_DMA_BUF_EXTENSION_NAME) == 1;
                        }
                        else
                                delete device;
                }
        }
};

VulkanIO::VulkanIO() : pImpl(std::make_unique<impl>())
{
}
VulkanIO::VulkanIO(const char *appName, uint32_t appVersion, std::vector<vk::PhysicalDeviceType> gpuTypes)
    : applicationName(appName), applicationVersion(appVersion), gpuTypes(gpuTypes), pImpl(std::make_unique<impl>())
{
}
VulkanIO::~VulkanIO() { Shutdown(); }

bool VulkanIO::Init() { return pImpl->Init(this); }

void VulkanIO::Shutdown()
{
}

void VulkanIO::PollDisplays()
{
        pImpl->PollDisplays();
}

bool VulkanIO::IsValid()
{
        return pImpl->IsValid();
}

vk::Instance singleton_instance;

} // namespace vkc