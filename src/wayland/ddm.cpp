/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "wayland.hpp"

namespace vkc
{

namespace
{

void create_data_source(struct wl_client *client,
                        struct wl_resource *resource,
                        uint32_t id)
{
        return;
}

void device_start_drag(struct wl_client *client,
                       struct wl_resource *resource,
                       struct wl_resource *source,
                       struct wl_resource *origin,
                       struct wl_resource *icon,
                       uint32_t serial)
{
        return;
}

void device_set_selection(struct wl_client *client,
                          struct wl_resource *resource,
                          struct wl_resource *source,
                          uint32_t serial)
{
        return;
}

void device_release(struct wl_client *client,
                    struct wl_resource *resource)
{
        return;
}

static const struct wl_data_device_interface data_device_interface = {device_start_drag, device_set_selection, device_release};

void get_data_device(struct wl_client *client,
                     struct wl_resource *resource,
                     uint32_t id,
                     struct wl_resource *seat)
{
        void *wseat =
            static_cast<Wayland::Seat *>(wl_resource_get_user_data(seat));
        Wayland::data_device_manager::data_device *device = new Wayland::data_device_manager::data_device();

        if (device == NULL)
        {
                wl_client_post_no_memory(client);
                return;
        }

        device->seat = wseat;

        device->resource = wl_resource_create(client, &wl_data_device_interface, 1, id);
        if (device->resource == NULL)
        {
                wl_client_post_no_memory(client);
                return;
        }

        wl_resource_set_implementation(device->resource, &data_device_interface, device, [](wl_resource *resource) { delete static_cast<Wayland::data_device_manager::data_device *>(wl_resource_get_user_data(resource)); });
}

static const struct wl_data_device_manager_interface data_device_manager_interface = {create_data_source, get_data_device};

static void
bind(struct wl_client *client, void *data,
     uint32_t version, uint32_t id)
{
        // struct weston_compositor *compositor = data;
        struct wl_resource *resource;

        resource = wl_resource_create(client, &wl_data_device_manager_interface,
                                      version, id);
        if (resource == NULL)
        {
                wl_client_post_no_memory(client);
                return;
        }

        wl_resource_set_implementation(resource, &data_device_manager_interface, data, nullptr);
}

} // namespace

bool Wayland::data_device_manager::Init()
{
        return singleton_wayland->GlobalCreate(&wl_data_device_manager_interface, 1, nullptr, &bind);
}

} // namespace vkc
