/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include <wayland.hpp>

#include <libdrm/drm_fourcc.h>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/epoll.h>
#include <string.h>
#include <stdlib.h>
#include <csignal>

extern "C"
{

        /*
 * Copyright © 2012 Collabora, Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

        int
        os_fd_set_cloexec(int fd)
        {
                long flags;

                if (fd == -1)
                        return -1;

                flags = fcntl(fd, F_GETFD);
                if (flags == -1)
                        return -1;

                if (fcntl(fd, F_SETFD, flags | FD_CLOEXEC) == -1)
                        return -1;

                return 0;
        }

        static int
        set_cloexec_or_close(int fd)
        {
                if (os_fd_set_cloexec(fd) != 0)
                {
                        close(fd);
                        return -1;
                }
                return fd;
        }

        int
        os_socketpair_cloexec(int domain, int type, int protocol, int *sv)
        {
                int ret;

#ifdef SOCK_CLOEXEC
                ret = socketpair(domain, type | SOCK_CLOEXEC, protocol, sv);
                if (ret == 0 || errno != EINVAL)
                        return ret;
#endif

                ret = socketpair(domain, type, protocol, sv);
                if (ret < 0)
                        return ret;

                sv[0] = set_cloexec_or_close(sv[0]);
                sv[1] = set_cloexec_or_close(sv[1]);

                if (sv[0] != -1 && sv[1] != -1)
                        return 0;

                close(sv[0]);
                close(sv[1]);
                return -1;
        }

        int
        os_epoll_create_cloexec(void)
        {
                int fd;

#ifdef EPOLL_CLOEXEC
                fd = epoll_create1(EPOLL_CLOEXEC);
                if (fd >= 0)
                        return fd;
                if (errno != EINVAL)
                        return -1;
#endif

                fd = epoll_create(1);
                return set_cloexec_or_close(fd);
        }

        static int
        create_tmpfile_cloexec(char *tmpname)
        {
                int fd;

#ifdef HAVE_MKOSTEMP
                fd = mkostemp(tmpname, O_CLOEXEC);
                if (fd >= 0)
                        unlink(tmpname);
#else
                fd = mkstemp(tmpname);
                if (fd >= 0)
                {
                        fd = set_cloexec_or_close(fd);
                        unlink(tmpname);
                }
#endif

                return fd;
        }

        /*
 * Create a new, unique, anonymous file of the given size, and
 * return the file descriptor for it. The file descriptor is set
 * CLOEXEC. The file is immediately suitable for mmap()'ing
 * the given size at offset zero.
 *
 * The file should not have a permanent backing store like a disk,
 * but may have if XDG_RUNTIME_DIR is not properly implemented in OS.
 *
 * The file name is deleted from the file system.
 *
 * The file is suitable for buffer sharing between processes by
 * transmitting the file descriptor over Unix sockets using the
 * SCM_RIGHTS methods.
 *
 * If the C library implements posix_fallocate(), it is used to
 * guarantee that disk space is available for the file at the
 * given size. If disk space is insufficient, errno is set to ENOSPC.
 * If posix_fallocate() is not supported, program may receive
 * SIGBUS on accessing mmap()'ed file contents instead.
 */
        int
        os_create_anonymous_file(off_t size)
        {
                static const char templat[] = "/weston-shared-XXXXXX";
                const char *path;
                char *name;
                int fd;
                int ret;

                path = getenv("XDG_RUNTIME_DIR");
                if (!path)
                {
                        errno = ENOENT;
                        return -1;
                }

                name = (char *)malloc(strlen(path) + sizeof(templat));
                if (!name)
                        return -1;

                strcpy(name, path);
                strcat(name, templat);

                fd = create_tmpfile_cloexec(name);

                free(name);

                if (fd < 0)
                        return -1;

#ifdef HAVE_POSIX_FALLOCATE
                do
                {
                        ret = posix_fallocate(fd, 0, size);
                } while (ret == EINTR);
                if (ret != 0)
                {
                        close(fd);
                        errno = ret;
                        return -1;
                }
#else
                do
                {
                        ret = ftruncate(fd, size);
                } while (ret < 0 && errno == EINTR);
                if (ret < 0)
                {
                        close(fd);
                        return -1;
                }
#endif

                return fd;
        }
}

namespace vkc
{

bool Wayland::Init()
{
        os_fd_set_cloexec(fileno(stdin));

        display = wl_display_create();

        loop = wl_display_get_event_loop(display);
        wl_event_loop_signal_func_t on_term_signal = [](int signal_number, void *data) -> int {
                // EventDispatch::emit(EventTermSignal(signal_number, data));
                return 0;
        };
#if 0
        signals[0] = wl_event_loop_add_signal(loop, SIGTERM, on_term_signal,
                                              display);
        signals[1] = wl_event_loop_add_signal(loop, SIGINT, on_term_signal,
                                              display);
        signals[2] = wl_event_loop_add_signal(loop, SIGQUIT, on_term_signal,
                                              display);

        signals[3] = wl_event_loop_add_signal(loop, SIGCHLD,
                                              [](int signal_number, void *data) -> int {
                                                      EventDispatch::emit(EventChildSignal());
                                                      return 0;
                                              },
                                              NULL);
        assert(signals[0] && signals[1] && signals[2] && signals[3]);
#endif

        // wl_display_add_socket_auto(display);
        wl_display_add_socket(display, "wayland-12");

        wl_display_init_shm(display);
        return m_Compositor.Init() &&
               m_Output.Init() &&
               m_data_device_manager.Init() &&
               m_Subcompositor.Init() &&
               m_zxdg_shell_v6.Init() &&
               m_Seat.Init() &&
               m_Shell.Init() &&
               true;
#if 0
        if (!)
                return false;
#endif
}

bool Wayland::GlobalCreate(const wl_interface *interface, int version, void *data, wl_global_bind_func_t bind)
{
        return wl_global_create(display, interface, version, data, bind);
}

void Wayland::Loop()
{
        wl_display_flush_clients(display);
        wl_event_loop_dispatch(loop, 0);
}

Wayland *singleton_wayland;

} // namespace vkc