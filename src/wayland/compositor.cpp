/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "wayland.hpp"

#include <map>

namespace vkc
{

Wayland_Compositor_Surface::active::buffer *
linux_dmabuf_buffer_get(struct wl_resource *resource);

namespace
{

static void
surface_destroy(struct wl_client *client, struct wl_resource *resource)
{
        wl_resource_destroy(resource);
}

#ifndef container_of
#define container_of(ptr, type, member) ({				\
	const __typeof__( ((type *)0)->member ) *__mptr = (ptr);	\
	(type *)( (char *)__mptr - offsetof(type,member) ); })
#endif

static void
buffer_destroy_handler(struct wl_listener *listener, void *data)
{
        Wayland_Compositor_Surface::active::buffer *buffer =
            container_of(listener, Wayland_Compositor_Surface::active::buffer, destroy_listener);

        wl_signal_emit(&buffer->destroy_signal, buffer);
        delete buffer;
}

Wayland_Compositor_Surface::active::buffer *
buffer_from_resource(struct wl_resource *resource)
{
        Wayland_Compositor_Surface::active::buffer *buffer;
        struct wl_listener *listener;

        listener = wl_resource_get_destroy_listener(resource,
                                                    buffer_destroy_handler);

        if (listener)
                return container_of(listener, Wayland_Compositor_Surface::active::buffer,
                                    destroy_listener);

        buffer = new Wayland_Compositor_Surface::active::buffer();
        if (buffer == NULL)
                return NULL;

        buffer->resource = resource;
        wl_signal_init(&buffer->destroy_signal);
        buffer->destroy_listener.notify = buffer_destroy_handler;
        // buffer->y_inverted = 1;
        wl_resource_add_destroy_listener(resource, &buffer->destroy_listener);

        return buffer;
}

static void
surface_state_set_buffer(Wayland_Compositor_Surface::active *state,
                         Wayland_Compositor_Surface::active::buffer *buffer)
{
        if (state->m_buffer == buffer)
                return;

        if (state->m_buffer)
                wl_list_remove(&state->buffer_destroy_listener.link);
        state->m_buffer = buffer;
        if (state->m_buffer)
                wl_signal_add(&state->m_buffer->destroy_signal,
                              &state->buffer_destroy_listener);
}

static void
surface_attach(struct wl_client *client,
               struct wl_resource *resource,
               struct wl_resource *buffer_resource, int32_t sx, int32_t sy)
{
        auto surface = static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(resource));
        Wayland_Compositor_Surface::active::buffer *buffer = nullptr;

        if (!buffer_resource)
        {
                /* Attach, attach, without commit in between does not send
                 * wl_buffer.release. */
                surface_state_set_buffer(&surface->pending, buffer);

                surface->pending.sx = sx;
                surface->pending.sy = sy;
                surface->pending.newly_attached = true;

                return;
        }

        buffer = linux_dmabuf_buffer_get(buffer_resource);
        if (!buffer)
        {
                buffer = buffer_from_resource(buffer_resource);
                if (buffer == NULL)
                {
                        wl_client_post_no_memory(client);
                        return;
                }
        }

        /* Attach, attach, without commit in between does not send
	 * wl_buffer.release. */
        surface_state_set_buffer(&surface->pending, buffer);

        surface->pending.sx = sx;
        surface->pending.sy = sy;
        surface->pending.newly_attached = true;
}

static void
surface_damage(struct wl_client *client,
               struct wl_resource *resource,
               int32_t x, int32_t y, int32_t width, int32_t height)
{
        auto surface = static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(resource));

        if (width <= 0 || height <= 0)
                return;

        surface->pending.m_damage_surface.insert(surface->pending.m_damage_surface.end(), (damage){x, y, width, height});
}

static void
surface_damage_buffer(struct wl_client *client,
                      struct wl_resource *resource,
                      int32_t x, int32_t y, int32_t width, int32_t height)
{
        auto surface = static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(resource));

        if (width <= 0 || height <= 0)
                return;

        surface->pending.m_damage_buffer.insert(surface->pending.m_damage_buffer.end(), (damage){x, y, width, height});
}

static void
surface_frame(struct wl_client *client,
              struct wl_resource *resource, uint32_t callback)
{
        auto surface = static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(resource));

        Wayland_Compositor_Surface::active::frame *cb = new Wayland_Compositor_Surface::active::frame();
        if (cb == NULL)
        {
                wl_resource_post_no_memory(resource);
                return;
        }

        cb->resource = wl_resource_create(client, &wl_callback_interface, 1,
                                          callback);
        if (cb->resource == NULL)
        {
                delete cb;
                wl_resource_post_no_memory(resource);
                return;
        }

        wl_resource_set_implementation(cb->resource, NULL, cb,
                                       [](wl_resource *resource) { delete static_cast<Wayland_Compositor_Surface::active::frame *>(wl_resource_get_user_data(resource)); });

        surface->pending.m_frames.insert(surface->pending.m_frames.end(), cb);
}

static void
surface_set_opaque_region(struct wl_client *client,
                          struct wl_resource *resource,
                          struct wl_resource *region_resource)
{
        auto surface = static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(resource));

        if (region_resource)
        {
                auto region = static_cast<Wayland::Compositor::Region *>(wl_resource_get_user_data(region_resource));
                surface->pending.opaque = region->m_additions;
        }
        else
        {
                surface->pending.opaque = (region_changelist){};
        }
}

static void
surface_set_input_region(struct wl_client *client,
                         struct wl_resource *resource,
                         struct wl_resource *region_resource)
{
        auto surface = static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(resource));

        if (region_resource)
        {
                auto region = static_cast<Wayland::Compositor::Region *>(wl_resource_get_user_data(region_resource));
                surface->pending.input = region->m_additions;
        }
        else
        {
                surface->pending.input = (region_changelist){};
        }
}

vk::Format formatWayland2Vulkan(wl_shm_format format)
{
        switch (format)
        {
        case WL_SHM_FORMAT_ABGR8888:
                return vk::Format::eR8G8B8A8Unorm;
        case WL_SHM_FORMAT_XBGR8888:
                return vk::Format::eR8G8B8A8Unorm;
        default:
                assert("Some error regarding shm image format.");
                return vk::Format::eR8G8B8A8Unorm;
        }
}

static void
surface_commit(struct wl_client *client, struct wl_resource *resource)
{
        auto surface = static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(resource));

        if (!surface->pending.m_buffer)
                return;

        auto buffer_resource = surface->pending.m_buffer->resource;
        switch (surface->pending.m_buffer->type)
        {
        case Wayland_Compositor_Surface::active::buffer::buffer_type::eSHM:
        {
                auto buffer = wl_shm_buffer_get(buffer_resource);
                wl_shm_buffer_begin_access(buffer);
                singleton_render->SurfaceCommit(surface, surface->pending.changed,
                                                wl_shm_buffer_get_data(buffer),
                                                wl_shm_buffer_get_stride(buffer),
                                                formatWayland2Vulkan(
                                                    static_cast<wl_shm_format>(wl_shm_buffer_get_format(buffer))),
                                                wl_shm_buffer_get_width(buffer),
                                                wl_shm_buffer_get_height(buffer),
                                                surface->pending.sx,
                                                surface->pending.sy,
                                                surface->pending.newly_attached,
                                                surface->pending.m_damage_surface,
                                                surface->pending.m_damage_buffer,
                                                surface->pending.opaque,
                                                surface->pending.input,
                                                surface->pending.transform,
                                                surface->pending.scale);
                wl_shm_buffer_end_access(buffer);
                wl_resource_queue_event(buffer_resource, WL_BUFFER_RELEASE);
                break;
        }
        case Wayland_Compositor_Surface::active::buffer::buffer_type::eDMABUF:
                singleton_render->SurfaceCommit(surface, surface->pending.changed,
                                                surface->pending.m_buffer->attributes.width,
                                                surface->pending.m_buffer->attributes.height,
                                                surface->pending.m_buffer->attributes.format,
                                                surface->pending.m_buffer->attributes.flags,
                                                surface->pending.m_buffer->attributes.n_planes,
                                                surface->pending.m_buffer->attributes.fd,
                                                surface->pending.m_buffer->attributes.offset,
                                                surface->pending.m_buffer->attributes.stride,
                                                surface->pending.m_buffer->attributes.modifier,
                                                surface->pending.sx,
                                                surface->pending.sy,
                                                surface->pending.newly_attached,
                                                surface->pending.m_damage_surface,
                                                surface->pending.m_damage_buffer,
                                                surface->pending.opaque,
                                                surface->pending.input,
                                                surface->pending.transform,
                                                surface->pending.scale);
                wl_resource_queue_event(buffer_resource, WL_BUFFER_RELEASE);
                break;
        default:
                assert(false && "Invalid buffer type");
        }

        surface->pending.changed = false;
        surface->pending.newly_attached = false;
        surface->pending.m_damage_buffer = (damage_list){};
        surface->pending.m_damage_surface = (damage_list){};

#if 0
        struct weston_subsurface *sub = weston_surface_to_subsurface(surface);

        if (!weston_surface_is_pending_viewport_source_valid(surface))
        {
                assert(surface->viewport_resource);

                wl_resource_post_error(surface->viewport_resource,
                                       WP_VIEWPORT_ERROR_OUT_OF_BUFFER,
                                       "wl_surface@%d has viewport source outside buffer",
                                       wl_resource_get_id(resource));
                return;
        }

        if (!weston_surface_is_pending_viewport_dst_size_int(surface))
        {
                assert(surface->viewport_resource);

                wl_resource_post_error(surface->viewport_resource,
                                       WP_VIEWPORT_ERROR_BAD_SIZE,
                                       "wl_surface@%d viewport dst size not integer",
                                       wl_resource_get_id(resource));
                return;
        }

        if (sub)
        {
                weston_subsurface_commit(sub);
                return;
        }

        weston_surface_commit(surface);

        wl_list_for_each(sub, &surface->subsurface_list, parent_link)
        {
                if (sub->surface != surface)
                        weston_subsurface_parent_commit(sub, 0);
        }
#endif
}

static void
surface_set_buffer_transform(struct wl_client *client,
                             struct wl_resource *resource, int transform)
{
        auto surface = static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(resource));

        /* if wl_output.transform grows more members this will need to be updated. */
        if (transform < 0 ||
            transform > WL_OUTPUT_TRANSFORM_FLIPPED_270)
        {
                wl_resource_post_error(resource,
                                       WL_SURFACE_ERROR_INVALID_TRANSFORM,
                                       "buffer transform must be a valid transform "
                                       "('%d' specified)",
                                       transform);
                return;
        }

        surface->pending.transform = transform;
        surface->pending.changed = true;
}

static void
surface_set_buffer_scale(struct wl_client *client,
                         struct wl_resource *resource,
                         int32_t scale)
{
        auto surface = static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(resource));

        if (scale < 1)
        {
                wl_resource_post_error(resource,
                                       WL_SURFACE_ERROR_INVALID_SCALE,
                                       "buffer scale must be at least one "
                                       "('%d' specified)",
                                       scale);
                return;
        }

        surface->pending.scale = scale;
        surface->pending.changed = true;
}

static const struct wl_surface_interface surface_interface = {
    surface_destroy,
    surface_attach,
    surface_damage,
    surface_frame,
    surface_set_opaque_region,
    surface_set_input_region,
    surface_commit,
    surface_set_buffer_transform,
    surface_set_buffer_scale,
    surface_damage_buffer};

void compositor_create_surface(struct wl_client *client, struct wl_resource *resource, uint32_t id)
{
        auto wc = static_cast<Wayland::Compositor *>(wl_resource_get_user_data(resource));

        auto surface = new Wayland_Compositor_Surface();
        if (surface == NULL)
        {
                wl_resource_post_no_memory(resource);
                return;
        }

        surface->resource =
            wl_resource_create(client, &wl_surface_interface,
                               wl_resource_get_version(resource), id);
        if (surface->resource == NULL)
        {
                delete surface;
                wl_resource_post_no_memory(resource);
                return;
        }

        singleton_render->InsertSurface(surface);

        wl_resource_set_implementation(surface->resource, &surface_interface,
                                       surface, [](wl_resource *resource) {
                                               auto surface = static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(resource));
                                               singleton_render->RemoveSurface(surface);
                                               delete surface;
                                       });

        // wl_signal_emit(&ec->create_surface_signal, surface);
}

void region_destroy(struct wl_client *client,
                    struct wl_resource *resource)
{
        wl_resource_destroy(resource);
}

void region_add(struct wl_client *client,
                struct wl_resource *resource,
                int32_t x,
                int32_t y,
                int32_t width,
                int32_t height)
{
        auto region = static_cast<Wayland::Compositor::Region *>(wl_resource_get_user_data(resource));
        region->m_additions.insert(region->m_additions.end(), (region_addition){false, x, y, width, height});
}

void region_subtract(struct wl_client *client,
                     struct wl_resource *resource,
                     int32_t x,
                     int32_t y,
                     int32_t width,
                     int32_t height)
{
        auto region = static_cast<Wayland::Compositor::Region *>(wl_resource_get_user_data(resource));
        region->m_additions.insert(region->m_additions.end(), (region_addition){true, x, y, width, height});
}

static const struct wl_region_interface region_interface = {region_destroy, region_add, region_subtract};

void compositor_create_region(struct wl_client *client, struct wl_resource *resource, uint32_t id)
{
#if 0
	struct weston_region *region;

	region = malloc(sizeof *region);
	if (region == NULL) {
		wl_resource_post_no_memory(resource);
		return;
	}

	pixman_region32_init(&region->region);

	region->resource =
		wl_resource_create(client, &wl_region_interface, 1, id);
	if (region->resource == NULL) {
		free(region);
		wl_resource_post_no_memory(resource);
		return;
	}
	wl_resource_set_implementation(region->resource, &region_interface,
				       region, destroy_region);
#else
        void *data =
            wl_resource_get_user_data(resource);
        auto region = new Wayland::Compositor::Region();

        if (region == NULL)
        {
                wl_client_post_no_memory(client);
                return;
        }

        region->resource = wl_resource_create(client, &wl_region_interface, 1, id);
        if (region->resource == NULL)
        {
                wl_client_post_no_memory(client);
                return;
        }

        wl_resource_set_implementation(region->resource, &region_interface, region, [](wl_resource *resource) { delete static_cast<Wayland::Compositor::Region *>(wl_resource_get_user_data(resource)); });
#endif
}

static const struct wl_compositor_interface compositor_interface = {
    compositor_create_surface,
    compositor_create_region};

void bind(struct wl_client *client,
          void *data, uint32_t version, uint32_t id)
{
        // struct weston_compositor *compositor = data;
        struct wl_resource *resource;

        resource = wl_resource_create(client, &wl_compositor_interface,
                                      version, id);
        if (resource == NULL)
        {
                wl_client_post_no_memory(client);
                return;
        }

        wl_resource_set_implementation(resource, &compositor_interface, data, nullptr);
}

} // namespace

Wayland_Compositor_Surface::active::buffer::buffer(Wayland_Compositor_Surface::active::buffer *b)
{
        this->type = buffer_type::eDMABUF;
        this->resource = b->resource;
        this->params_resource = nullptr;
        this->attributes = b->attributes;

        wl_signal_init(&this->destroy_signal);
        this->destroy_listener.notify = buffer_destroy_handler;
        wl_resource_add_destroy_listener(resource, &this->destroy_listener);
}

bool Wayland::Compositor::Init()
{
        return singleton_wayland->GlobalCreate(&wl_compositor_interface, 4, this, &bind);
}

} // namespace vkc