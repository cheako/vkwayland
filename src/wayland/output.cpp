/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "wayland.hpp"

namespace vkc
{

namespace
{

void release(struct wl_client *client,
             struct wl_resource *resource)
{
        return;
}

static const struct wl_output_interface output_interface = {release};

static void
bind(struct wl_client *client, void *data,
     uint32_t version, uint32_t id)
{
        // struct weston_compositor *compositor = data;
        struct wl_resource *resource;

        resource = wl_resource_create(client, &wl_output_interface,
                                      version, id);
        if (resource == NULL)
        {
                wl_client_post_no_memory(client);
                return;
        }

        wl_resource_set_implementation(resource, &output_interface, data, nullptr);

        wl_output_send_geometry(resource, 0, 0, 0, 0, WL_OUTPUT_SUBPIXEL_UNKNOWN, "Vulkan", "", WL_OUTPUT_TRANSFORM_NORMAL);
        wl_output_send_mode(resource, WL_OUTPUT_MODE_CURRENT | WL_OUTPUT_MODE_PREFERRED, 1920, 1080, 60027);
        wl_output_send_scale(resource, 1);
        wl_output_send_done(resource);
}

} // namespace

bool Wayland::Output::Init()
{
        return singleton_wayland->GlobalCreate(&wl_output_interface, 2, nullptr, &bind);
}

} // namespace vkc
