/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#pragma once

#include "device.hpp"

namespace vkc
{

class Render::Device::Surface : public std::enable_shared_from_this<Render::Device::Surface>
{
      public:
        ~Surface();

        bool Init(Device *device);

        void Shutdown();

        void Commit(
            bool changed,
            void *data,
            int32_t stride,
            vk::Format format,
            int32_t width,
            int32_t height,
            int32_t sx,
            int32_t sy,
            bool newly_attached,
            damage_list damage_surface,
            damage_list damage_buffer,
            region_changelist opaque,
            region_changelist input,
            int transform,
            int32_t scale);

        void Commit(bool changed,
                    int32_t width,
                    int32_t height,
                    std::pair<vk::Format, bool> format,
                    uint32_t flags,
                    int n_planes,
                    int *fd,
                    uint32_t *offset,
                    uint32_t *stride,
                    uint64_t *modifier,
                    int32_t sx,
                    int32_t sy,
                    bool newly_attached,
                    damage_list damage_surface,
                    damage_list damage_buffer,
                    region_changelist opaque,
                    region_changelist input,
                    int transform,
                    int32_t scale);

        vk::Result status = vk::Result::eErrorInitializationFailed;

        vk::Buffer m_buffer;
        vk::ImageView m_textureImageView;
        vk::Buffer m_uniformBuffer;
        uint32_t m_offset;
        uint32_t m_stride;
        vk::Format m_format;
        bool m_byteshift = false;
        int32_t m_width;
        int32_t m_height;
        vk::Image m_textureImage;
        std::vector<vk::BufferImageCopy> m_regions;
        vk::DescriptorSet m_descriptorSet;

      private:
        Device *device;
        VmaAllocation m_allocation;
        VmaAllocation m_uniformAllocation;
        VmaAllocation m_textureAllocation;
        int32_t m_sx;
        int32_t m_sy;
        bool m_newly_attached;
        damage_list m_damage_surface;
        damage_list m_damage_buffer;
        region_changelist m_opaque;
        region_changelist m_input;
        int m_transform;
        int32_t m_scale;
        vk::Image m_importImage;
        vk::DeviceMemory m_importMemory;
        vk::ImageView m_importView;

        void CleanupAttached();

        bool CreateUniformBuffer();

        bool CreateDescriptorSet();
};

} // namespace vkc