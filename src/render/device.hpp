/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#pragma once

#include "render.hpp"

namespace vkc
{

class Render::Device
{
      public:
        ~Device();

        bool Init(vk::PhysicalDevice p, vk::Device d, uint32_t familyIndex, std::set<std::string> device_extentions);

        void Shutdown();

        vk::Result status = vk::Result::eErrorInitializationFailed;

        class OutSurface;
        typedef std::map<vk::SurfaceKHR, OutSurface *> Ac;
        Ac m_surfaces;

        bool InsertSurface(vk::SurfaceKHR s);
        void RemoveSurface(vk::SurfaceKHR s);

        class Surface;
        std::map<vkc::Wayland_Compositor_Surface *, Surface *> m_buffers;
        std::set<Surface *> m_outstandingBuffers;

        void InsertSurface(Wayland_Compositor_Surface *surface);
        void RemoveSurface(Wayland_Compositor_Surface *surface);
        void SurfaceCommit(Wayland_Compositor_Surface *surface,
                           bool changed,
                           void *data,
                           int32_t stride,
                           vk::Format format,
                           int32_t width,
                           int32_t height,
                           int32_t sx,
                           int32_t sy,
                           bool newly_attached,
                           damage_list damage_surface,
                           damage_list damage_buffer,
                           region_changelist opaque,
                           region_changelist input,
                           int transform,
                           int32_t scale);

        void SurfaceCommit(Wayland_Compositor_Surface *surface,
                           bool changed,
                           int32_t width,
                           int32_t height,
                           std::pair<vk::Format, bool> format,
                           uint32_t flags,
                           int n_planes,
                           int *fd,
                           uint32_t *offset,
                           uint32_t *stride,
                           uint64_t *modifier,
                           int32_t sx,
                           int32_t sy,
                           bool newly_attached,
                           damage_list damage_surface,
                           damage_list damage_buffer,
                           region_changelist opaque,
                           region_changelist input,
                           int transform,
                           int32_t scale);

        bool InitDMABUF();

        bool Frame();

        class Vertex;
        class Image;
        class Buffer;
        class Shader;

        vk::PhysicalDevice m_physical;
        vk::Device m_logical;
        vk::CommandBuffer m_commandBuffer;
        Buffer *m_vertexBuffer;
        Shader *m_vertexShader;
        Shader *m_fragmentShader;
        vk::DescriptorSetLayout m_descriptorLayouts[2];
        vk::DescriptorPool m_descriptorPool;
        vk::PipelineLayout m_pipelineLayout;
        vk::PipelineCache m_pipelineCache;
        VmaAllocator m_allocator;

      private:
        uint32_t m_familyIndex;
        std::set<std::string> m_device_extentions;
        vk::Queue m_queue;
        vk::CommandPool m_commandPool;
        std::vector<vk::Semaphore> m_imageAvailableSemaphores;
        std::vector<vk::SwapchainKHR> m_swapchains;
        vk::Fence m_fence;
        vk::Semaphore m_renderDoneSemaphore;
        vk::Sampler m_sampler;

        bool CreatePrimaryBuffer();

        bool CreateCommandPool();

        bool CreateVertexBuffer();

        bool CreatePipeline();

        bool CreateDescriptorLayout();

        bool CreateDescriptorPool();

        bool CreateVMA();

        bool CreateShaders();

        bool CreateSampler();

        bool CreateFence();

        bool CreateSemaphore();

        bool GetSurfaceSupportKHR(vk::SurfaceKHR s);
        bool CreateSurfaceSwapchain(vk::SurfaceKHR s, vk::Extent2D *e);
        bool CreateSurfaceSemaphore();
};

} // namespace vkc