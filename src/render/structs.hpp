/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#pragma once

#include "device.hpp"

namespace vkc
{

struct Render::Device::Vertex
{
        struct VertexData::VertexPos pos;
        struct VertexData::VertexUV uv;
        static vk::VertexInputBindingDescription const s_inputBindingDescription;
        static vk::VertexInputAttributeDescription const s_inputAttributeDescription[];
};

class Render::Device::Image
{
      public:
        vk::Image image;
        vk::DeviceMemory memory;
        vk::ImageView view;
};

class Render::Device::Buffer
{
      public:
        bool Stage(Render::Device &device, void const *data, size_t size, vk::BufferUsageFlagBits usage);

        vk::Buffer buffer;
        VmaAllocation allocation;

      private:
        bool CreateBuffer(Render::Device &device, size_t size, vk::BufferUsageFlagBits usage);

        bool CopyMemory(Render::Device &device, void const *data, size_t size);
};

class Render::Device::Shader
{
      public:
        bool Init(Render::Device &device, std::string name, std::string _code, vk::ShaderStageFlagBits stage);

        vk::Result state = vk::Result::eErrorInitializationFailed;
        vk::ShaderStageFlagBits stage;
        vk::ShaderModule shaderModule;
        vk::PipelineShaderStageCreateInfo shaderStage;
};

} // namespace vkc