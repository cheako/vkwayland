/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#pragma once

#include "device.hpp"

namespace vkc
{

class Render::Device::OutSurface : public std::enable_shared_from_this<Render::Device::OutSurface>
{
      public:
        ~OutSurface() { Shutdown(); }

        bool Init(Device *_device, vk::SurfaceKHR surface, vk::SwapchainKHR swapchain, vk::Extent2D imageSize, vk::Semaphore imageAvailableSemaphore);

        void Shutdown();

        bool Frame();

        vk::Result status = vk::Result::eErrorInitializationFailed;

        vk::Semaphore m_imageAvailableSemaphore;
        vk::SwapchainKHR m_swapchain;
        uint32_t m_currentFrameBuffer;

      private:
        Device *device;
        vk::SurfaceKHR m_surface;
        vk::Extent2D m_imageSize;
        class NextImage
        {
              public:
                vk::Image m_image;
                vk::ImageView m_imageView;
                vk::Image m_depthStencil;
                VmaAllocation m_depthStencilAllocation;
                vk::ImageView m_depthStencilView;
                vk::Framebuffer m_framebuffer;
        };
        std::vector<NextImage> m_nextImages;
        vk::RenderPass m_renderPass;
        vk::Pipeline m_pipeline;

        vk::Buffer m_uniformBuffer;
        VmaAllocation m_uniformAllocation;
        vk::DescriptorSet m_descriptorSet;

        bool CreateRenderPass();

        bool CreateSwapchainImageViews();

        bool CreatePipeline();

        bool CreateDescriptorSet();

        bool CreateUniformBuffer();
};

} // namespace vkc