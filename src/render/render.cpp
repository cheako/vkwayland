/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */

#include "device.hpp"

namespace vkc
{

bool Render::Init() { return true; }

void Render::Shutdown()
{
        m_devices.clear();
}

bool Render::CreateDevice(vk::PhysicalDevice physical, vk::Device logical, uint32_t familyIndex, std::set<std::string> extentions)
{
        m_devices.insert(std::make_pair(logical, std::make_shared<Device>()));
        return m_devices[logical]->Init(physical, logical, familyIndex, extentions);
}

bool Render::InsertSurface(vk::Device logical, vk::SurfaceKHR surface) { return m_devices[logical]->InsertSurface(surface); }
void Render::RemoveSurface(vk::Device logical, vk::SurfaceKHR surface) { m_devices[logical]->RemoveSurface(surface); };

void Render::InsertSurface(Wayland_Compositor_Surface *surface)
{
        for (auto it : m_devices)
                it.second->InsertSurface(surface);
}
void Render::RemoveSurface(Wayland_Compositor_Surface *surface)
{
        for (auto it : m_devices)
                it.second->RemoveSurface(surface);
}

bool Render::Frame()
{
        bool ret = true;
        for (auto d : m_devices)
                ret = d.second->Frame() && ret;
        return ret;
}

void Render::SurfaceCommit(Wayland_Compositor_Surface *surface, bool changed,
                           void *data, int32_t stride, vk::Format format, int32_t width,
                           int32_t height, int32_t sx, int32_t sy, bool newly_attached,
                           damage_list damage_surface, damage_list damage_buffer,
                           region_changelist opaque, region_changelist input,
                           int transform, int32_t scale)
{
        for (auto it : m_devices)
                it.second->SurfaceCommit(
                    surface, changed, data, stride, format, width,
                    height, sx, sy, newly_attached, damage_surface,
                    damage_buffer, opaque, input, transform, scale);
}

void Render::SurfaceCommit(Wayland_Compositor_Surface *surface,
                           bool changed, int32_t width, int32_t height,
                           std::pair<vk::Format, bool> format, uint32_t flags, int n_planes,
                           int *fd, uint32_t *offset, uint32_t *stride, uint64_t *modifier,
                           int32_t sx, int32_t sy, bool newly_attached, damage_list damage_surface,
                           damage_list damage_buffer, region_changelist opaque,
                           region_changelist input, int transform, int32_t scale)
{
        for (auto it : m_devices)
                it.second->SurfaceCommit(
                    surface, changed, width, height, format, flags,
                    n_planes, fd, offset, stride, modifier, sx, sy,
                    newly_attached, damage_surface, damage_buffer,
                    opaque, input, transform, scale);
}

std::set<uint64_t> Render::DRMFormatModifiers(vk::Format format)
{
        std::set<uint64_t> modifyers = {};
        std::set<uint64_t> next = {};
        bool first = true;
        for (auto it : m_devices)
        {
                auto ml = it.second->m_physical.getFormatProperties2<vk::FormatProperties2, vk::DrmFormatModifierPropertiesListEXT>(format).get<vk::DrmFormatModifierPropertiesListEXT>();
                for (size_t i = 0; i < ml.drmFormatModifierCount; i++)
                {
                        if (!first)
                        {
                                if (modifyers.count(ml.pDrmFormatModifierProperties[i].drmFormatModifier) != 0)
                                        next.insert(next.begin(), ml.pDrmFormatModifierProperties[i].drmFormatModifier);
                        }
                        else
                                next.insert(next.begin(), ml.pDrmFormatModifierProperties[i].drmFormatModifier);
                }
                modifyers = next;
                first = false;
        }
        return modifyers;
}

bool Render::InitDMABUF()
{
        for (auto it : m_devices)
                if (!it.second->InitDMABUF())
                        return false;
        return true;
}

Render::~Render() { Shutdown(); }

Render *singleton_render;

} // namespace vkc
