/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include <vulkanio.hpp>
#include "outsurface.hpp"
#include "surface.hpp"
#include <iostream>
#include <unistd.h>

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/string_cast.hpp>

namespace vkc
{

bool Render::Device::Surface::Init(Device *_device)
{
        device = _device;

        return true;
}

void Render::Device::Surface::Shutdown()
{
        if (m_descriptorSet)
        {
                status = device->m_logical.freeDescriptorSets(device->m_descriptorPool, m_descriptorSet);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "Warning: Failed to free descriptor set." << std::endl;
                }
        }

        if (m_uniformBuffer && m_uniformAllocation)
                vmaDestroyBuffer(device->m_allocator, m_uniformBuffer, m_uniformAllocation);
        if (m_buffer && m_allocation)
                vmaDestroyBuffer(device->m_allocator, m_buffer, m_allocation);
        if (m_textureImageView)
                device->m_logical.destroyImageView(m_textureImageView);
        if (m_textureImage && m_textureAllocation)
                vmaDestroyImage(device->m_allocator, m_textureImage, m_textureAllocation);
}

void Render::Device::Surface::CleanupAttached()
{
        if (m_buffer && m_allocation)
                vmaDestroyBuffer(device->m_allocator, m_buffer, m_allocation);
        m_allocation = nullptr;
        m_buffer = vk::Buffer();

        if (m_importView)
                device->m_logical.destroyImageView(m_importView);
        m_importView = vk::ImageView();

        if (m_importMemory)
                device->m_logical.freeMemory(m_importMemory);
        m_importMemory = vk::DeviceMemory();

        if (m_importImage)
                device->m_logical.destroyImage(m_importImage);
        m_importImage = vk::Image();
}

const static vk::ImageSubresourceRange subresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);
void Render::Device::Surface::Commit(
    bool changed,
    void *data,
    int32_t stride,
    vk::Format format,
    int32_t width,
    int32_t height,
    int32_t sx,
    int32_t sy,
    bool newly_attached,
    damage_list damage_surface,
    damage_list damage_buffer,
    region_changelist opaque,
    region_changelist input,
    int transform,
    int32_t scale)
{
        if (newly_attached)
        {
                if (!m_descriptorSet &&
                    !(CreateDescriptorSet() &&
                      CreateUniformBuffer()))
                        return;

                CleanupAttached();

                if (m_textureImageView)
                        device->m_logical.destroyImageView(m_textureImageView);

                if (m_textureImage && m_textureAllocation)
                        vmaDestroyImage(device->m_allocator, m_textureImage, m_textureAllocation);

                m_stride = stride;
                m_format = format;
                m_width = width;
                m_height = height;
                m_sx = sx;
                m_sy = sy;
                m_offset = reinterpret_cast<uintptr_t>(data) % 32;

                { /* Resize the uniform model Matrix. */
                        glm::mat4 modelMat = glm::translate(glm::vec3((float)sx - 15.f, (float)sy + 32.f, 0.5f));
                        modelMat = glm::scale(modelMat, glm::vec3((float)width, (float)height, 0.f));

                        glm::mat4 *uniform;
                        vmaMapMemory(device->m_allocator, m_uniformAllocation, reinterpret_cast<void **>(&uniform));
                        *uniform = modelMat;
                        vmaUnmapMemory(device->m_allocator, m_uniformAllocation);
                }
                {
                        vk::BufferCreateInfo bufferInfo;
                        bufferInfo.setSize(stride * height + m_offset);
                        bufferInfo.setUsage(vk::BufferUsageFlagBits::eTransferSrc);

                        VkBufferCreateInfo tmpInfo;
                        tmpInfo = bufferInfo;

                        VmaAllocationCreateInfo bufferAllocInfo = {.usage = VMA_MEMORY_USAGE_CPU_TO_GPU};

                        VkBuffer tmpBuffer;
                        status = vk::Result(vmaCreateBuffer(device->m_allocator, &tmpInfo, &bufferAllocInfo, &tmpBuffer, &m_allocation, nullptr));
                        if (status != vk::Result::eSuccess)
                        {
                                std::cerr << "Failed to create uniform buffer." << std::endl;
                        }
                        m_buffer = tmpBuffer;
                }
                {
                        vk::ImageCreateInfo imageInfo;
                        imageInfo.setImageType(vk::ImageType::e2D);
                        imageInfo.setExtent(vk::Extent3D(width, height, 1));
                        imageInfo.setMipLevels(1);
                        imageInfo.setArrayLayers(1);

                        imageInfo.setFormat(format);
                        imageInfo.setTiling(vk::ImageTiling::eOptimal);
                        imageInfo.setInitialLayout(vk::ImageLayout::eUndefined);
                        imageInfo.setUsage(vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled);
                        imageInfo.setSamples(vk::SampleCountFlagBits::e1);

                        VkImageCreateInfo tmpImageInfo;
                        tmpImageInfo = imageInfo;

                        VmaAllocationCreateInfo allocInfo = {};
                        allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                        VkImage imageTemp;
                        status = vk::Result(vmaCreateImage(
                            device->m_allocator,
                            &tmpImageInfo, &allocInfo, &imageTemp,
                            &m_textureAllocation, nullptr));
                        if (status != vk::Result::eSuccess)
                        {
                                throw std::runtime_error("failed to create image!");
                        }
                        m_textureImage = imageTemp;
                }
                {
                        vk::ImageViewCreateInfo viewInfo;
                        viewInfo.setImage(m_textureImage);
                        viewInfo.setViewType(vk::ImageViewType::e2D);
                        viewInfo.setFormat(m_format);
                        viewInfo.setSubresourceRange(subresourceRange);

                        std::tie(status, m_textureImageView) = device->m_logical.createImageView(viewInfo);
                        if (status != vk::Result::eSuccess)
                        {
                                throw std::runtime_error("failed to allocate texture view!");
                        }
                }
                {
                        vk::DescriptorImageInfo descriptorImageInfo;
                        descriptorImageInfo.setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
                        descriptorImageInfo.setImageView(m_textureImageView);
                        descriptorImageInfo.setSampler(device->m_sampler);

                        vk::WriteDescriptorSet writeInfo;
                        writeInfo.setDescriptorCount(1);
                        writeInfo.setDstBinding(1);
                        writeInfo.setDescriptorType(vk::DescriptorType::eCombinedImageSampler);
                        writeInfo.setPImageInfo(&descriptorImageInfo);
                        writeInfo.setDstSet(m_descriptorSet);
                        device->m_logical.updateDescriptorSets(writeInfo, nullptr);
                }
        }

        if (changed)
        {
                m_transform = transform;
                m_scale = scale;
        }

        char *buffer_data;
        {
                status = vk::Result(vmaMapMemory(device->m_allocator, m_allocation, reinterpret_cast<void **>(&buffer_data)));
                if (status != vk::Result::eSuccess)
                {
                        throw std::runtime_error("failed to map image memory!");
                }

                memcpy(buffer_data + m_offset, data, m_stride * m_height);
                vmaUnmapMemory(device->m_allocator, m_allocation);
                vmaFlushAllocation(device->m_allocator, m_allocation, 0, m_stride * m_height);
        }
#if 0
        /* Collect data for testing. */
        {
                char filename[] = "image_data.XXXXXX";
                int fd = mkstemp(filename);
                if (fd != -1)
                {
                        write(fd, data, m_stride * m_height);
                        close(fd);
                }
        }
        {
                char filename[] = "image_info.XXXXXX";
                int fd = mkstemp(filename);
                if (fd != -1)
                {
                        dprintf(fd, "uint32_t stride = %d;\n"
                                    "uint32_t format = %d;\n"
                                    "uint32_t width = %d;\n"
                                    "uint32_t height = %d;\n"
                                    "uint32_t sx = %d;\n"
                                    "uint32_t sy = %d;\n"
                                    "uint32_t transform = %d;\n"
                                    "uint32_t scale = %d;\n",
                                m_stride,
                                m_format,
                                m_width,
                                m_height,
                                m_sx,
                                m_sy,
                                m_transform,
                                m_scale);
                        close(fd);
                }
        }
#endif

        {
                const vk::ImageSubresourceLayers subresourceLayers(vk::ImageAspectFlagBits::eColor, 0, 0, 1);

                vk::BufferImageCopy region;
                region.setImageOffset(m_offset);
                region.setBufferRowLength(m_stride / 4);
                region.setImageSubresource(subresourceLayers);

                if (damage_buffer.empty())
                {
                        region.setImageOffset(vk::Offset3D(0, 0, 0));
                        region.setImageExtent(vk::Extent3D(
                            m_width,
                            m_height,
                            1));

                        m_regions.push_back(region);
                }
                else
                {
                        for (auto it : damage_buffer)
                        {
                                region.setImageOffset(vk::Offset3D(it.x, it.y, 0));
                                region.setImageExtent(vk::Extent3D(
                                    it.width,
                                    it.height,
                                    1));

                                m_regions.push_back(region);
                        }
                }
                device->m_outstandingBuffers.insert(this);
        }
}

void Render::Device::Surface::Commit(bool changed,
                                     int32_t width,
                                     int32_t height,
                                     std::pair<vk::Format, bool> format,
                                     uint32_t flags,
                                     int n_planes,
                                     int *fd,
                                     uint32_t *offset,
                                     uint32_t *stride,
                                     uint64_t *modifier,
                                     int32_t sx,
                                     int32_t sy,
                                     bool newly_attached,
                                     damage_list damage_surface,
                                     damage_list damage_buffer,
                                     region_changelist opaque,
                                     region_changelist input,
                                     int transform,
                                     int32_t scale)
{
        if (newly_attached)
        {
                if (!m_descriptorSet &&
                    !(CreateDescriptorSet() &&
                      CreateUniformBuffer()))
                        return;

                CleanupAttached();

                if (m_textureImageView)
                        device->m_logical.destroyImageView(m_textureImageView);

                if (m_textureImage && m_textureAllocation)
                        vmaDestroyImage(device->m_allocator, m_textureImage, m_textureAllocation);

                vk::DispatchLoaderDynamic dldid(singleton_instance, device->m_logical);

                m_offset = *offset;
                m_stride = *stride;
                m_format = format.first;
                m_byteshift = format.second;
                m_width = width;
                m_height = height;
                m_sx = sx;
                m_sy = sy;

                { /* Resize the uniform model Matrix. */
                        glm::mat4 modelMat = glm::translate(glm::vec3((float)sx - 15.f + 400, (float)sy + 32.f + 300, 0.5f));
                        modelMat = glm::scale(modelMat, glm::vec3((float)width, (float)height, 0.f));

                        glm::mat4 *uniform;
                        vmaMapMemory(device->m_allocator, m_uniformAllocation, reinterpret_cast<void **>(&uniform));
                        *uniform = modelMat;
                        vmaUnmapMemory(device->m_allocator, m_uniformAllocation);
                }
                {
                        vk::SubresourceLayout subLayout;
                        subLayout.offset = 0;
                        subLayout.size = m_stride * m_height;
                        subLayout.rowPitch = m_stride;
                        subLayout.arrayPitch = subLayout.size;
                        subLayout.depthPitch = subLayout.size;

                        vk::StructureChain<vk::ImageCreateInfo, vk::ExternalMemoryImageCreateInfo, vk::ImageDrmFormatModifierExplicitCreateInfoEXT> drmImageInfo;
                        drmImageInfo.get<vk::ExternalMemoryImageCreateInfo>().setHandleTypes(
                            vk::ExternalMemoryHandleTypeFlags(vk::ExternalMemoryHandleTypeFlagBits::eDmaBufEXT));
                        drmImageInfo.get<vk::ImageDrmFormatModifierExplicitCreateInfoEXT>().setDrmFormatModifier(*modifier);
                        drmImageInfo.get<vk::ImageDrmFormatModifierExplicitCreateInfoEXT>().setDrmFormatModifierPlaneCount(1);
                        drmImageInfo.get<vk::ImageDrmFormatModifierExplicitCreateInfoEXT>().setPPlaneLayouts(&subLayout);

                        vk::ImageCreateInfo imageInfo;
                        imageInfo.setPNext(&drmImageInfo.get<vk::ExternalMemoryImageCreateInfo>());
                        imageInfo.setImageType(vk::ImageType::e2D);
                        imageInfo.setExtent(vk::Extent3D(width, height, 1));
                        imageInfo.setMipLevels(1);
                        imageInfo.setArrayLayers(1);
                        imageInfo.setFormat(m_format);
                        imageInfo.setTiling(vk::ImageTiling::eLinear);
                        imageInfo.setInitialLayout(vk::ImageLayout::ePreinitialized);
                        imageInfo.setUsage(vk::ImageUsageFlagBits::eTransferSrc);
                        imageInfo.setSamples(vk::SampleCountFlagBits::e1);

                        std::tie(status, m_importImage) = device->m_logical.createImage(imageInfo);
                        if (status != vk::Result::eSuccess)
                        {
                                throw std::runtime_error("failed to create dmabuf image!");
                        }
                }
                {
                        vk::MemoryFdPropertiesKHR memoryFdProp;
                        std::tie(status, memoryFdProp) = device->m_logical.getMemoryFdPropertiesKHR(vk::ExternalMemoryHandleTypeFlagBits::eDmaBufEXT, *fd, dldid);
                        if (status != vk::Result::eSuccess)
                        {
                                throw std::runtime_error("failed to getMemoryFdPropertiesKHR()!");
                        }

                        uint32_t memoryType = 1;
                        {
                                size_t m = 1;
                                while (!(memoryFdProp.memoryTypeBits & m))
                                {
                                        m = m << 1;
                                        memoryType++;
                                        assert(memoryType <= 32);
                                }
                        }
                        {
                                vk::MemoryRequirements memRequirements;
                                memRequirements = device->m_logical.getImageMemoryRequirements(m_importImage);

                                vk::StructureChain<vk::MemoryAllocateInfo, vk::ImportMemoryFdInfoKHR, vk::MemoryDedicatedAllocateInfo> allocInfo;
                                allocInfo.get<vk::MemoryAllocateInfo>().setAllocationSize(memRequirements.size);
                                allocInfo.get<vk::MemoryAllocateInfo>().setMemoryTypeIndex(memoryType - 1);

                                allocInfo.get<vk::ImportMemoryFdInfoKHR>().setHandleType(vk::ExternalMemoryHandleTypeFlagBits::eDmaBufEXT);
                                allocInfo.get<vk::ImportMemoryFdInfoKHR>().setFd(dup(*fd));

                                allocInfo.get<vk::MemoryDedicatedAllocateInfo>().setImage(m_importImage);

                                std::tie(status, m_importMemory) = device->m_logical.allocateMemory(allocInfo.get<vk::MemoryAllocateInfo>());
                                if (status != vk::Result::eSuccess)
                                {
                                        throw std::runtime_error("failed to import dmabuf!");
                                }

                                status = device->m_logical.bindImageMemory(m_importImage, m_importMemory, m_offset);
                                if (status != vk::Result::eSuccess)
                                {
                                        throw std::runtime_error("failed to bind dmabuf!");
                                }
                        }
                }
                { /* Here this implementation strays from best,
                   * there is no need for the following "temperary storage"
                   * */
                        vk::ImageCreateInfo imageInfo;
                        imageInfo.setImageType(vk::ImageType::e2D);
                        imageInfo.setExtent(vk::Extent3D(width, height, 1));
                        imageInfo.setMipLevels(1);
                        imageInfo.setArrayLayers(1);
                        imageInfo.setFormat(m_format);
                        imageInfo.setSamples(vk::SampleCountFlagBits::e1);
                        imageInfo.setTiling(vk::ImageTiling::eOptimal);
                        imageInfo.setInitialLayout(vk::ImageLayout::eUndefined);
                        imageInfo.setUsage(vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled);

                        VkImageCreateInfo tmpImageInfo;
                        tmpImageInfo = imageInfo;

                        VmaAllocationCreateInfo vmaAllocInfo = {};
                        vmaAllocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                        VkImage imageTemp;
                        status = vk::Result(vmaCreateImage(
                            device->m_allocator,
                            &tmpImageInfo, &vmaAllocInfo, &imageTemp,
                            &m_textureAllocation, nullptr));
                        if (status != vk::Result::eSuccess)
                        {
                                throw std::runtime_error("failed to create image!");
                        }
                        m_textureImage = imageTemp;
                }
                {
                        vk::ImageViewCreateInfo viewInfo;
                        viewInfo.setImage(m_textureImage);
                        viewInfo.setViewType(vk::ImageViewType::e2D);
                        viewInfo.setFormat(m_format);
                        viewInfo.setSubresourceRange(subresourceRange);

                        std::tie(status, m_textureImageView) = device->m_logical.createImageView(viewInfo);
                        if (status != vk::Result::eSuccess)
                        {
                                throw std::runtime_error("failed to allocate texture view!");
                        }
                }
                {
                        vk::DescriptorImageInfo descriptorImageInfo;
                        descriptorImageInfo.setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
                        descriptorImageInfo.setImageView(m_textureImageView);
                        descriptorImageInfo.setSampler(device->m_sampler);

                        vk::WriteDescriptorSet writeInfo;
                        writeInfo.setDescriptorCount(1);
                        writeInfo.setDstBinding(1);
                        writeInfo.setDescriptorType(vk::DescriptorType::eCombinedImageSampler);
                        writeInfo.setPImageInfo(&descriptorImageInfo);
                        writeInfo.setDstSet(m_descriptorSet);
                        device->m_logical.updateDescriptorSets(writeInfo, nullptr);
                }
        }

        if (changed)
        {
                m_transform = transform;
                m_scale = scale;
        }

        const vk::ImageSubresourceLayers subresourceLayers(vk::ImageAspectFlagBits::eColor, 0, 0, 1);

        std::vector<vk::ImageCopy> regions;
        {
                vk::ImageCopy region;
                region.setSrcSubresource(subresourceLayers);
                region.setDstSubresource(subresourceLayers);

                if (damage_buffer.empty())
                {
                        region.setExtent(vk::Extent3D(
                            m_width,
                            m_height,
                            1));

                        regions.push_back(region);
                }
                else
                {
                        for (auto it : damage_buffer)
                        {
                                region.setExtent(vk::Extent3D(
                                    it.width,
                                    it.height,
                                    1));

                                regions.push_back(region);
                        }
                }
        }
        { /* The unwanted copy, we can't defer because after this 
           * function returns the Wayland Buffer is Released.
           */
                vk::CommandBufferBeginInfo beginInfo;
                beginInfo.setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
                device->m_commandBuffer.begin(beginInfo);

                vk::ImageMemoryBarrier importBarrier;
                importBarrier.setOldLayout(vk::ImageLayout::ePreinitialized);
                importBarrier.setNewLayout(vk::ImageLayout::eTransferSrcOptimal);
                importBarrier.setDstAccessMask(vk::AccessFlagBits::eTransferRead);
                importBarrier.setSubresourceRange(subresourceRange);
                importBarrier.setImage(m_importImage);

                vk::ImageMemoryBarrier barrier;
                barrier.setOldLayout(vk::ImageLayout::eUndefined);
                barrier.setNewLayout(vk::ImageLayout::eTransferDstOptimal);
                barrier.setDstAccessMask(vk::AccessFlagBits::eTransferWrite);
                barrier.setSubresourceRange(subresourceRange);
                barrier.setImage(m_textureImage);

                device->m_commandBuffer.pipelineBarrier(
                    vk::PipelineStageFlagBits::eTopOfPipe,
                    vk::PipelineStageFlagBits::eTransfer,
                    vk::DependencyFlags(0),
                    nullptr, nullptr, {barrier, importBarrier});

                device->m_commandBuffer.copyImage(
                    m_importImage,
                    vk::ImageLayout::eTransferSrcOptimal,
                    m_textureImage,
                    vk::ImageLayout::eTransferDstOptimal,
                    regions);
                regions.clear();

                barrier.setOldLayout(vk::ImageLayout::eTransferDstOptimal);
                barrier.setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
                barrier.setSrcAccessMask(vk::AccessFlagBits::eTransferWrite);
                barrier.setDstAccessMask(vk::AccessFlagBits::eShaderRead);

                device->m_commandBuffer.pipelineBarrier(
                    vk::PipelineStageFlagBits::eTransfer,
                    vk::PipelineStageFlagBits::eFragmentShader,
                    vk::DependencyFlags(0),
                    nullptr, nullptr, barrier);

                device->m_commandBuffer.end();
        }
        {
                vk::SubmitInfo submitInfo;
                submitInfo.setCommandBufferCount(1);
                submitInfo.setPCommandBuffers(&device->m_commandBuffer);

                status = device->m_queue.submit(1, &submitInfo, device->m_fence);
                if (status != vk::Result::eSuccess)
                {
                        throw("Failed to submit image copy cmd");
                }
        }
        /* This blocks untill the m_queue.submit() and image copy completes. */
        device->m_logical.waitForFences(device->m_fence, false, UINT64_MAX);
        device->m_logical.resetFences(device->m_fence);
}

Render::Device::Surface::~Surface() { Shutdown(); }

bool Render::Device::Surface::CreateDescriptorSet()
{
        vk::DescriptorSetAllocateInfo allocInfo;
        allocInfo.setDescriptorPool(device->m_descriptorPool);
        allocInfo.setDescriptorSetCount(1);
        allocInfo.setPSetLayouts(&device->m_descriptorLayouts[0]);

        std::vector<vk::DescriptorSet> sets;
        std::tie(status, sets) = device->m_logical.allocateDescriptorSets(allocInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create render done semaphore." << std::endl;
                return false;
        }

        m_descriptorSet = sets[0];

        return true;
}

bool Render::Device::Surface::CreateUniformBuffer()
{
        vk::BufferCreateInfo imageInfo;
        imageInfo.setSize(sizeof(glm::mat4));
        imageInfo.setUsage(vk::BufferUsageFlagBits::eUniformBuffer);

        VkBufferCreateInfo tmpInfo;
        tmpInfo = imageInfo;

        VmaAllocationCreateInfo bufferAllocInfo = {};
        bufferAllocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
        bufferAllocInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

        VkBuffer tmpBuffer;
        status = vk::Result(vmaCreateBuffer(device->m_allocator, &tmpInfo, &bufferAllocInfo, &tmpBuffer, &m_uniformAllocation, nullptr));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create uniform buffer." << std::endl;
                return false;
        }

        m_uniformBuffer = tmpBuffer;

        glm::mat4 *uniform;
        vmaMapMemory(device->m_allocator, m_uniformAllocation, reinterpret_cast<void **>(&uniform));
        *uniform = glm::mat4();
        vmaUnmapMemory(device->m_allocator, m_uniformAllocation);

        vk::DescriptorBufferInfo descriptorBufferInfo;
        descriptorBufferInfo.setBuffer(m_uniformBuffer);
        descriptorBufferInfo.setRange(sizeof(glm::mat4));

        vk::WriteDescriptorSet writeInfo;
        writeInfo.setDescriptorCount(1);
        writeInfo.setDstSet(m_descriptorSet);
        writeInfo.setDescriptorType(vk::DescriptorType::eUniformBuffer);
        writeInfo.setPBufferInfo(&descriptorBufferInfo);
        device->m_logical.updateDescriptorSets(writeInfo, nullptr);

        return true;
}

} // namespace vkc