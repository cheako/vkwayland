/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "inputdriverimpl.hpp"

namespace vkc
{

InputDriver::InputDriver() : pImpl(std::make_unique<impl>())
{
}
InputDriver::~InputDriver()
{
}

bool InputDriver::Init() { return pImpl->Init(this); }

void InputDriver::Shutdown()
{
}

void InputDriver::PollEvents()
{
        pImpl->PollEvents();
}

InputDriver::impl *singleton_inputdriver;

} // namespace vkc