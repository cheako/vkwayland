/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "inputdriver.hpp"
#include <GLFW/glfw3.h>

namespace vkc
{

extern InputDriver::impl *singleton_inputdriver;

class InputDriver::impl
{
      public:
        bool Init(InputDriver *t)
        {
                m_this = t;
                singleton_inputdriver = this;

                return true;
        }

        void PollEvents()
        {
                glfwPollEvents();
        }

        bool InsertWindow(GLFWwindow *window)
        {
                m_data_window = window;
                glfwSetKeyCallback(window, KeyCallback);
                return false;
        }

        bool RemoveWindow(GLFWwindow *window)
        {
                return false;
        }

      private:
        InputDriver *m_this;
        GLFWwindow *m_data_window;

        static void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
        {
        }
};

}