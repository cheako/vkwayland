/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include <vulkanio.hpp>
#include <render.hpp>
#include <wayland.hpp>

#include "inputdriverimpl.hpp"

#include <iostream>

namespace vkc
{

class VulkanIO::impl
{
      public:
        ~impl() { Shutdown(); };

        bool Init(VulkanIO *v)
        {
                m_this = v;
                return CreateInstance() && CreateSurface() && FindPhysicalDevice() && InsertSurface();
        }

        void Shutdown()
        {
                if (m_logical)
                        m_logical.destroy();

                if (m_surface)
                        singleton_instance.destroySurfaceKHR(m_surface);

                if (m_data_window)
                        glfwDestroyWindow(m_data_window);

                if (singleton_instance)
                        singleton_instance.destroy();
        }

        void PollDisplays()
        {
        }

        bool IsValid() { return !glfwWindowShouldClose(m_data_window); }

      private:
        VulkanIO *m_this;
        vk::Device m_logical;
        GLFWwindow *m_data_window;
        vk::SurfaceKHR m_surface;

        bool CreateInstance()
        {
                VulkanIO *v = m_this;

                glfwSetErrorCallback([](int error, const char *description) {
                        fprintf(stderr, "Error %d: %s\n", error, description);
                });

                assert(glfwInit());
                if (!glfwVulkanSupported())
                {
                        std::cerr << "Vulkan is not supported" << std::endl;
                        return false;
                }

                uint32_t glfwExtCount = 0;
                const char **glfwExts = glfwGetRequiredInstanceExtensions(&glfwExtCount);
                std::vector<char const *> enabledInstanceExtensions(glfwExts, glfwExts + glfwExtCount);
                std::vector<char const *> enabledInstanceLayers{};
#ifndef NDEBUG
                enabledInstanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
                enabledInstanceLayers.push_back("VK_LAYER_LUNARG_standard_validation");
#endif

                vk::ApplicationInfo appInfo;
                appInfo.setApiVersion(VK_API_VERSION_1_1);
                appInfo.setApplicationVersion(v->applicationVersion);
                appInfo.setPApplicationName(v->applicationName.c_str());
                appInfo.setPEngineName("VkWaylandGLFW");
                appInfo.setEngineVersion(VK_MAKE_VERSION(0, 0, 1));

                vk::InstanceCreateInfo instanceCreateInfo;
                instanceCreateInfo.setPApplicationInfo(&appInfo);
                instanceCreateInfo.setEnabledLayerCount(static_cast<uint32_t>(enabledInstanceLayers.size()));
                instanceCreateInfo.setPpEnabledLayerNames(enabledInstanceLayers.data());
                instanceCreateInfo.setEnabledExtensionCount(static_cast<uint32_t>(enabledInstanceExtensions.size()));
                instanceCreateInfo.setPpEnabledExtensionNames(enabledInstanceExtensions.data());

                std::tie(v->status, singleton_instance) = vk::createInstance(instanceCreateInfo);
                if (v->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to createsingleton_instance." << std::endl;
                        return false;
                }

                return true;
        }

        bool CreateSurface()
        {
                glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
                glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
                m_data_window = glfwCreateWindow(800, 600,
                                                 m_this->applicationName.c_str(), NULL, NULL);

                VkSurfaceKHR surface;
                VkResult err = glfwCreateWindowSurface(singleton_instance, m_data_window, NULL, &surface);
                assert(err == VK_SUCCESS && "glfwCreateWindowSurface: Failed.");

                m_surface = vk::SurfaceKHR(surface);

                return true;
        }

        bool FindPhysicalDevice()
        {
                VulkanIO *v = m_this;
                std::vector<vk::PhysicalDevice> physicalDevices;
                std::tie(v->status, physicalDevices) = singleton_instance.enumeratePhysicalDevices();
                if (v->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to find Vulkan capable devices." << std::endl;
                        return false;
                }

                auto const queueFamilyProperties = physicalDevices[0].getQueueFamilyProperties();

                for (uint32_t i = 0; i < queueFamilyProperties.size(); ++i)
                {
                        if ((queueFamilyProperties[i].queueFlags & vk::QueueFlagBits::eGraphics) && glfwGetPhysicalDevicePresentationSupport(singleton_instance, physicalDevices[0], i))
                        {
                                return CreateLogicalDevice(physicalDevices[0], i);
                        }
                }

                v->status = vk::Result::eErrorInitializationFailed;
                std::cerr << "Failed to find discrete gpu." << std::endl;
                return false;
        }

        bool CreateLogicalDevice(vk::PhysicalDevice physical, uint32_t familyIndex)
        {
                VulkanIO *v = m_this;
                vk::DeviceQueueCreateInfo deviceQueueCreateInfo;
                float const priority = 1.0f;
                deviceQueueCreateInfo.setPQueuePriorities(&priority);
                deviceQueueCreateInfo.setQueueCount(1);
                deviceQueueCreateInfo.setQueueFamilyIndex(familyIndex);

                std::vector<char const *> wantExtensionNames = {
                    VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME,
                    VK_KHR_MAINTENANCE1_EXTENSION_NAME,
                    VK_KHR_SAMPLER_YCBCR_CONVERSION_EXTENSION_NAME,
                    VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME,
                    VK_KHR_IMAGE_FORMAT_LIST_EXTENSION_NAME,
                    VK_KHR_BIND_MEMORY_2_EXTENSION_NAME,
                    VK_EXT_IMAGE_DRM_FORMAT_MODIFIER_EXTENSION_NAME,
                    VK_KHR_EXTERNAL_MEMORY_CAPABILITIES_EXTENSION_NAME,
                    VK_KHR_EXTERNAL_MEMORY_EXTENSION_NAME,
                    VK_KHR_EXTERNAL_MEMORY_FD_EXTENSION_NAME,
                    VK_EXT_EXTERNAL_MEMORY_DMA_BUF_EXTENSION_NAME,
                    VK_KHR_SWAPCHAIN_EXTENSION_NAME};

                std::vector<vk::ExtensionProperties> extentions;
                std::tie(v->status, extentions) = physical.enumerateDeviceExtensionProperties();
                if (v->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to enumerateDeviceExtensionProperties." << std::endl;
                        return false;
                }

                std::set<std::string> device_extentions;
                std::vector<char const *> deviceExtensionNames;
                for (std::vector<vk::ExtensionProperties>::const_iterator x = extentions.begin(); x != extentions.end(); ++x)
                        for (std::vector<char const *>::const_iterator y = wantExtensionNames.begin(); y != wantExtensionNames.end(); ++y)
                                if (0 == strcmp(x->extensionName, *y))
                                {
                                        deviceExtensionNames.push_back(*y);
                                        device_extentions.insert(*y);
                                }

                vk::DeviceCreateInfo deviceCreateInfo;
                deviceCreateInfo.setQueueCreateInfoCount(1);
                deviceCreateInfo.setPQueueCreateInfos(&deviceQueueCreateInfo);
                deviceCreateInfo.setEnabledExtensionCount(deviceExtensionNames.size());
                deviceCreateInfo.setPpEnabledExtensionNames(deviceExtensionNames.data());

                std::tie(v->status, m_logical) = physical.createDevice(deviceCreateInfo);
                if (v->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create logical device." << std::endl;
                        return false;
                }

                singleton_render->CreateDevice(physical, m_logical, familyIndex, device_extentions);

                if (device_extentions.count(VK_EXT_EXTERNAL_MEMORY_DMA_BUF_EXTENSION_NAME) == 1 && singleton_render->InitDMABUF())
                        singleton_wayland->m_DmaBuf.Init();

                return true;
        }

        bool InsertSurface()
        {
                singleton_render->InsertSurface(m_logical, m_surface);
                return true;
        }
};

VulkanIO::VulkanIO() : pImpl(std::make_unique<impl>())
{
}
VulkanIO::VulkanIO(const char *appName, uint32_t appVersion, std::vector<vk::PhysicalDeviceType> gpuTypes)
    : applicationName(appName), applicationVersion(appVersion), pImpl(std::make_unique<impl>())
{
}
VulkanIO::~VulkanIO() { Shutdown(); }

bool VulkanIO::Init() { return pImpl->Init(this); }

void VulkanIO::Shutdown()
{
}

void VulkanIO::PollDisplays()
{
        pImpl->PollDisplays();
}

bool VulkanIO::IsValid()
{
        return pImpl->IsValid();
}

vk::Instance singleton_instance;

} // namespace vkc