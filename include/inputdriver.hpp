/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#pragma once

#include <memory>
#include <experimental/propagate_const>

namespace vkc
{

class InputDriver
{
      public:
        InputDriver();

        ~InputDriver();

        bool Init();

        void PollEvents();

        class impl;
        std::experimental::propagate_const<std::unique_ptr<impl>> pImpl;

      private:
        void Shutdown();
};

} // namespace vkc