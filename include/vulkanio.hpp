/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#pragma once

#include "vulkan.hpp"

#include <vector>
#include <set>
#include <memory>
#include <experimental/propagate_const>

namespace vkc
{

class VulkanIO
{
      public:
        VulkanIO();

        VulkanIO(const char *appName, uint32_t appVersion, std::vector<vk::PhysicalDeviceType> gpuTypes);

        ~VulkanIO();

        bool Init();

        bool IsValid();

        void PollDisplays();

        std::string const applicationName = "VkWayland";
        uint32_t const applicationVersion = VK_MAKE_VERSION(0, 0, 1);
        std::vector<vk::PhysicalDeviceType> const gpuTypes{vk::PhysicalDeviceType::eDiscreteGpu};

        std::set<std::string> extentions;
        vk::Result status = vk::Result::eErrorInitializationFailed;

      private:
        void Shutdown();
        
        class impl;
        std::experimental::propagate_const<std::unique_ptr<impl>> pImpl;
};

extern vk::Instance singleton_instance;

} // namespace vkc